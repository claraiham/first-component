import './profile.css';
import connected from '../img/connected.jpeg';
import disconnected from '../img/disconnected.png';


 function Profile(props){

    return (
        <div>
            <div className='.Profile__img-container '>
                <img src={props.img} alt="image" className ="Profile__img-profile"/>
                <div className='state'>
                    {props.isConnected ? <img src={connected}/> : <img src={disconnected}/>}
                </div>
            </div> 
            <p>
                {props.name}
            </p>
        </div>
    );
    
}

export default Profile