import Profile from './components/profile'
import './App.css'

function App() {

  return (
    <>
      <Profile name="Arthur Pendragon" img= "https://images.rtl.fr/~c/2000v2000/rtl/www/1395587-alexandre-astier-dans-le-costume-du-roi-arthur.jpg" isConnected = {true} className ="Profile__img-profile-carree"/>
      <Profile name="Perceval le Gallois" img= "https://www.serieously.com/app/uploads/2021/08/sans-titre-57-18-600x0-c-default.jpg" isConnected = {false} className ="Profile__img-profile-rectangle"/>
      <Profile name="Karadoc de Vannes" img= "https://i.pinimg.com/originals/7a/19/2a/7a192aa92abf185bfd7a15d447263a24.jpg" isConnected = {true} className ="Profile__img-profile-rectangle"/>
    </>
  )
}

export default App
